import React from "react";
import "../App.css";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import PageTodoList from '../Component/PageTodoList'
import PageLogIn from '../Component/PageLogin'
import PageUserForm from "../Component/PageUserForm";

localStorage.setItem('islogin', false)
const PrivateRoute = ({ path, component: Component }) => (
  <Route path={path} render={(props) => {
    const islogin = localStorage.getItem('islogin')
    if (islogin == 'true')
      return <Component {...props} />
    else
      return <Redirect to="/" />
  }} />
)

const Rounter = () => {
  return (
    <div>
      <BrowserRouter>
        <Route path="/processlogin" render={() => {
          localStorage.setItem("islogin", true);
          return <Redirect to="/users" exact={true} />
        }} ></Route>
        <Route path="/processlogout" render={() => {
          localStorage.setItem("islogin", false);
          return <Redirect to="/login" />
        }} ></Route>
        <Route path="/" component={PageLogIn} exact={true} />
        <Route path="/users" component={PageUserForm} exact={true} />
        <Route path="/users/:user_id/todo" component={PageTodoList} />
      </BrowserRouter>
    </div>
  )
}

export default Rounter;
