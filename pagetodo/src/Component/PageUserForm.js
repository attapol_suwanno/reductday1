import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { List, Avatar, Skeleton, Button } from 'antd';
import { Layout, Breadcrumb } from 'antd';
import PageSearch from './PageSearch';
import { setId, fetchUser } from '../Action/PageUserAction'
import { bindActionCreators } from 'redux'
import { useHistory } from 'react-router';

const { Content } = Layout;

const PageTodoForm = (props) => {
    const history = useHistory();
    const { toDos, searchText, fetchUser } = props

    useEffect(() => {
        fetchUser();
    }, [])

    return (
        <div>
            <Layout>
                <Content style={{ padding: '0 50px', marginTop: 64 }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                    </Breadcrumb>
                    <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
                        <div>
                            <PageSearch />
                            <Button style={{ marginLeft: 1176 }} classname='buttonligth' value="Logout" onClick={() => { window.location = "/processlogout" }} >Logout</Button>
                        </div>
                        <List
                            className="demo-loadmore-list"
                            itemLayout="horizontal"
                            dataSource={
                                searchText == '' ?
                                    toDos
                                    :
                                    toDos.filter(m => m.name.match(searchText))
                            }
                            renderItem={item => (
                                <List.Item
                                    actions={[<a onClick={() => history.push('/users/' + item.id + '/todo')}>Todo</a>]}>
                                    <Skeleton avatar title={false} loading={item.loading} active>
                                        <List.Item.Meta
                                            avatar={<Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>U</Avatar>
                                            }
                                            title={<a onClick={() => history.push('/users/' + item.id + '/todo')}>{item.name}</a>}
                                            description={item.email}
                                        />
                                    </Skeleton>
                                </List.Item>
                            )}
                        />
                    </div>
                </Content>
            </Layout>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        toDos: state.todocompState.todos,
        searchText: state.todocompState.searchtext
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ setId, fetchUser }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PageTodoForm);