import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { setDataList } from '../Action/PageTodoAction'
import { Dropdown, Button, List, Typography } from 'antd';
import { Menu, Layout, Breadcrumb, PageHeader, Descriptions } from 'antd';
import { bindActionCreators } from 'redux';
import { fetchTODoList, setclickDone } from '../Action/PageTodoAction';
import PageSearch from './PageSearch'
import { fetchUser } from '../Action/PageUserAction'

const { Content } = Layout;

const PageTodoList = (props) => {
    const { listTodo, searchText, datauser } = props
    const { fetchTODoList, fetchUser, setclickDone } = props
    const userId = props.match.params.user_id;
    const [completTest, setcompletTest] = useState('All')

    useEffect(() => {
        fetchTODoList(userId);
        fetchUser(userId);
    }, [])

    const handleMenuClick = (datasave) => {
        setcompletTest(datasave.key)
    }

    const menu = (listTodo) => {
        let completFilter = [];
        listTodo = listTodo ? listTodo : []
        listTodo.map(datacomplet => {
            if (!completFilter.includes(datacomplet.completed))
                completFilter.push(datacomplet.completed)
        });
        return (
            <Menu onClick={handleMenuClick}>
                {completFilter.map((list) => {
                    return (
                        <Menu.Item value={list} key={list}>
                            {
                                list == true ?
                                    "Done"
                                    :
                                    "Doing"
                            }
                        </Menu.Item>
                    )
                })}
            </Menu>
        )
    }

    let dataAll = listTodo
    dataAll = searchText == '' ?
        dataAll
        :
        dataAll.filter(m => m.title.match(searchText));

    dataAll = completTest == 'All' ?
        dataAll
        :
        listTodo.filter((data) => {
            return (data.completed + "" == completTest + "")
        })

    if (!dataAll) dataAll = [];


    return (
        <div>
            <Layout>
                <Content style={{ padding: '0 50px', marginTop: 64 }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Todo</Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                        style={{ border: '1px solid rgb(235, 237, 240)', }}
                        onBack={() => window.history.back()}
                        title="Home"
                    />
                    <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
                        <div><Button style={{ marginLeft: 1176 }} value="Logout" onClick={() => { window.location = "/processlogout" }} >Logout</Button></div>

                        <Descriptions  >
                            {datauser.map((user) => {
                                return (
                                    <div>
                                        <Descriptions.Item label="Name">Name : {user.name}<br /></Descriptions.Item>
                                        <Descriptions.Item label="UserName">User Name : {user.username}<br /></Descriptions.Item>
                                        <Descriptions.Item label="Email">Email : {user.email}</Descriptions.Item>
                                    </div>)
                            })}
                        </Descriptions>

                        <div id="components-dropdown-demo-dropdown-button">
                            <Dropdown overlay={menu(dataAll)}>

                                <Button>
                                    {completTest == 'All' ?
                                        ('All')
                                        :
                                        completTest == true ?
                                            ("Done")
                                            :
                                            ("Doing")
                                    }
                                </Button>
                            </Dropdown>
                        </div>
                        <div>
                            <PageSearch />
                        </div>
                        <List
                            bordered
                            dataSource={dataAll}
                            renderItem={(item, index) => (
                                <div>
                                    {<List.Item>
                                        {
                                            item.completed == true ?
                                                <Typography.Text mark>Done</Typography.Text>
                                                :
                                                <Typography.Text delete>Doing</Typography.Text>
                                        }
                                        {item.title}
                                        {
                                            item.completed == true ?
                                                <Button className='buttonDo' >
                                                    Done
                                                </Button>
                                                :
                                                <Button className='buttonDo' type="primary" onClick={() => setclickDone(item.id)} >
                                                    Doing
                                        </Button>
                                        }
                                    </List.Item>
                                    }
                                </div>
                            )}
                        />
                    </div>
                </Content>
            </Layout>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        listTodo: state.dataTodo.list,
        searchText: state.dataTodo.searchtext,
        datauser: state.todocompState.todos

    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchTODoList, setDataList, fetchUser, setclickDone }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PageTodoList);