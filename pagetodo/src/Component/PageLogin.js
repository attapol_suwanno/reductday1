import React from 'react';
import { Layout, Input,Icon } from 'antd';
import { Button } from 'antd';
const { Header, Footer, Content } = Layout;
const PageLogIn = () => {
  return (
    <div>
      <div>
        <Layout>
          <Header>Login</Header>
          <Content>
            <h1 className='name'>Login</h1>
            <div className='name' ><Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" /></div>
            <div className='name'><Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Passworde " /></div>
            <div className='name'><Button type="primary" onClick={() => { window.location = "/processlogin" }} >Login</Button></div></Content>
          <Footer>
          </Footer>
        </Layout>
      </div>
    </div>
  )
}
export default PageLogIn;