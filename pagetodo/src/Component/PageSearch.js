
import React from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { setDataSearch } from '../Action/PageUserAction'
import { setDataListSearch } from '../Action/PageTodoAction'
import { bindActionCreators } from 'redux'
const { Search } = Input;

const PageSearch = (props) => {
  const { setDataSearch, setDataListSearch } = props

  const onSearchname = (event) => {
    setDataSearch(event.target.value)
    setDataListSearch(event.target.value)
  }

  return (
    <div>
      <Search
        placeholder="input search text"
        onChange={onSearchname}
        style={{ width: 200 }}
      />
    </div>
  )
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setDataSearch, setDataListSearch }, dispatch)
}
export default connect(null, mapDispatchToProps)(PageSearch);