import { combineReducers } from 'redux';
import {userReducer} from './ReducerUser'
import {todoListReducer} from './ReducerTodo'

export const rootReducer = combineReducers({
    todocompState: userReducer,
    dataTodo:todoListReducer
   
})