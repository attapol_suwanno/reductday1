const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN';
const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR';
const SET_DATASEARCH = 'SET_DATASEARCH';
const SET_DATALIST = 'SET_DATALIST';
const SET_CLICKDONE = 'SET_CLICKDONE';


const initialState = {
  list: '',
  loading: false,
  error: '',
  searchtext: ''
}

export const todoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODOS_BEGIN:
      return {
        ...state,
        loading: true
      }

    case FETCH_TODOS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }

    case SET_DATASEARCH:
      return {
        ...state,
        searchtext: action.payLoad
      }

    case SET_DATALIST:
      return {
        ...state,
        list: action.payLoad
      }

    case SET_CLICKDONE:
      const datachangButton = [...state.list]
      datachangButton[action.payLoad - 1].completed = true

      return {
        ...state,
        list: datachangButton
      }

    default:
      return state

  }
}





