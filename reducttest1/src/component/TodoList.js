import React from 'react';
import {useSelector} from 'react-redux';

const TodoList =(props)=>{

    const {todos}=props
    const todos =useSelector(state =>state.todos);
    return(
        <div>
            {todos.map((todo,index)=>
            <h4 key={index}>{todo.taskName}</h4>)}
        </div>
    )
}
const mapStateToProps = state => {
    return {
        todos: state.todos
    }

}
export default connect(mapStateToProps)(TodoList);