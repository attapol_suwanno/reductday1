import React, { useState } from 'react';
import {useDispatch} from 'react-redux';
import { addTodo } from '../action/TodoAction';

const TodoForm =()=>{
    const [newTodoName,setNewTodoName]=useState('');

    const dispatch =useDispatch();
    const clickedAddHandler=()=>{
        dispatch(addTodo(newTodoName));
        setNewTodoName('');
    }
    return(
        <div>
            <input value ={newTodoName} onChange={(e)=> setNewTodoName(e.target.value)}/>
            <button onClick={clickedAddHandler}>Add</button>
        </div>
    )
}
export default TodoForm;