import React from 'react';
import { useSelector, useDispatch, connect } from 'react-redux';
import { decrement, increment } from '../action/counterAction';
import{bindActionCreators } from 'redux';

const CounterRedux = (props) => {



    const { counter, increment, decrement } = props

    // const couter=useSelector(state =>state.counter)
    // const dispatch =useDispatch();
    return (
        <div>
            <h1>Counter Redux</h1>
            <div>counter={counter}</div>
            <div>
                {/* <button onClick={()=>dispatch(decrement())}>-</button>
            <button onClick={()=>dispatch(increment())}>+</button> */}

                <button onClick={() => decrement(5)}>-</button>
                <button onClick={() => increment(5)}>+</button>




            </div>
        </div>
    )
}




const mapStateToProps = state => {
    return {
        counter: state.counter
    }

}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ decrement, increment }, dispatch)

}
export default connect(mapStateToProps, mapDispatchToProps)(CounterRedux)




//map state dispath แบบ กำหนก แอคชั้นเพิ่มในนี้ได้ เพพียงใส่ในนี้ให้มันง่าสยต่อการเรียกใช้