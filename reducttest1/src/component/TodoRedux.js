import React, { useEffect } from 'react';
import {useDispatch} from 'react-redux';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import { fetchTodo } from '../action/TodoAction';
const TodoRedux = () => {
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(fetchTodo());
    },[]);
    
    return (
        <div>
           <div>
               <h1>Todo redux</h1>
               <TodoForm/>
               <TodoList/>

           </div>
        </div>
    )
}
export default TodoRedux; 