export const INCREAMENT ='INCREAMENT';
export const DECREMENT ='DECREMENT'

export const increment =(value)=>{
    const action={
    type: INCREAMENT,
    payLoad: value
}
    return action;
}

export const decrement =(value)=>{
   
    const action={
        type: DECREMENT,
        payLoad: value
    }
    return action;

    }
    
