
const initialCounst = 0;
export const counterReducer = (state = initialCounst, action) => {
    switch (action.type) {
        case 'INCREAMENT':
            return state + action.payLoad;
        case 'DECREMENT':
            return state - action.payLoad;
        default:
            return state
    }

}