import React from 'react';
import './App.css';
import Couter from './component/counter';
import CounterRedux from './component/counterRedux';
import TodoRedux from './component/TodoRedux';

function App() {
  return (
    <div>
      <Couter/>
      <CounterRedux/>
      <TodoRedux/>
    </div>
  );
}

export default App;
